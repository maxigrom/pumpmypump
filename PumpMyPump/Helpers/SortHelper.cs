﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PumpMyPump.Helpers
{
    public class SortHelper
    {

        public static MvcHtmlString Link(string controllerName, string text, string field, string viewBagSortBy, bool viewBagIsAsc)
        {
            field = field.ToLower();
            viewBagSortBy = viewBagSortBy.ToLower();

            bool isAsc = field != viewBagSortBy || !viewBagIsAsc;

            var url = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var routeParams = new RouteValueDictionary();

            foreach (string key in HttpContext.Current.Request.QueryString.Keys)
            {
                routeParams.Add(key, HttpContext.Current.Request.QueryString[key]);
            }

            routeParams["sortBy"] = field;
            routeParams["isAsc"] = isAsc.ToString().ToLower();

            var href = url.Action("Index", controllerName, routeParams);

            var returnString = string.Format("<a href='{0}'>{1}</a>", href, text);
            if (field == viewBagSortBy)
            {
                returnString += isAsc
                    ? "<span class='glyphicon glyphicon-arrow-down'></span>"
                    : "<span class='glyphicon glyphicon-arrow-up'></span>";
            }

            return new MvcHtmlString(returnString);
        }
    }
}