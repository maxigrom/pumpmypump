﻿using PumpMyPump.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PumpMyPump.Helpers
{
    public static class AuthHelper
    {
        public static bool Login(string email, string password)
        {
            var db = DataBase.Connect;
            var user = db.Users.FirstOrDefault(u => (string.Compare(u.Email, email, true) == 0) && 
                                                    (string.Compare(u.Password, Crypt(password), true) == 0));

            HttpContext.Current.Session["User"] = user;
            return user != null;
        }

        public static void Logout()
        {
            HttpContext.Current.Session["User"] = null;
        }

        public static bool IsLoginUser
        {
            get { return User != null; }
        }

        public static User User
        {
            get { return (User)HttpContext.Current.Session["User"]; }
        }

        public static string Crypt(string str)
        {
            byte[] inputBytes = Encoding.ASCII.GetBytes(str);
            byte[] hash = MD5.Create().ComputeHash(inputBytes);

            var sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        internal static bool UserExists(string email)
        {
            var db = DataBase.Connect;
            return db.Users.Any(u => string.Compare(u.Email, email, true) == 0);
        }

        internal static bool UserExists(string email, int id)
        {
            var db = DataBase.Connect;
            return db.Users.Any(u => string.Compare(u.Email, email, true) == 0 && u.Id != id);
        }
    }
}
