﻿using PumpMyPump.Models;
using System.Configuration;
using System.Reflection;

namespace PumpMyPump
{
    internal static class DataBase
    {
        public static DbDataContext Connect
        {
            get { return new DbDataContext(ConfigurationManager.ConnectionStrings["PumpDbConnectionString"].ConnectionString); }
        }
    }
}