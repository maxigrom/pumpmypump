﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using PumpMyPump.Models;
using PumpMyPump.Models.ViewModels;
using System.Data.Linq;
using PumpMyPump.Models.Types;

namespace PumpMyPump.Helpers
{
    public class AppHelper
    {
        public static IEnumerable<SelectListItem> GetExerciseList(int exerciseId)
        {
            var db = DataBase.Connect;
            var exercises = db.CurrentExercises.OrderBy(e => e.Name).ToList();

            if (exercises.FirstOrDefault(e => e.Id == exerciseId) == null)
            {
                return new SelectList(exercises, "Id", "Name");
            }

            return new SelectList(exercises, "Id", "Name", exerciseId);
        }

        public static IEnumerable<Exercise> Exercises
        {
            get
            {
                var db = DataBase.Connect;
                return db.CurrentExercises.OrderBy(e => e.Name).ToList();
            }
        }

        public static IEnumerable<SelectListItem> ExerciseList
        {
            get
            {
                var db = DataBase.Connect;
                return new SelectList(db.CurrentExercises.OrderBy(e => e.Name).ToList(), "Id", "Name");
            }
        }

        public static IEnumerable<SelectListItem> BodyPartList
        {
            get
            {
                var db = DataBase.Connect;
                return new SelectList(db.CurrentBodyParts.OrderBy(bp => bp.Name).ToList(), "Id", "Name");
            }
        }

        public static IEnumerable<SelectListItem> MuscleList
        {
            get
            {
                var db = DataBase.Connect;
                return new SelectList(db.CurrentMuscles.OrderBy(m => m.Name).ToList(), "Id", "Name");
            }
        }

        public static IEnumerable<SelectListItem> SportEquipList
        {
            get
            {
                var db = DataBase.Connect;
                return new SelectList(db.CurrentSportEquips.OrderBy(se => se.Name).ToList(), "Id", "Name");
            }
        }

        public static IEnumerable<SelectListItem> ScoreList
        {
            get { return new SelectList(ScoreType.GetList(), "Value", "Name"); }
        }

        public static IEnumerable<SelectListItem> LevelList
        {
            get { return new SelectList(LevelType.GetList(), "Value", "Name"); }
        }

        public static IEnumerable<SelectListItem> RoleList
        {
            get { return new SelectList(RoleType.GetList(), "Value", "Name"); }
        }

        public static IEnumerable<SelectListItem> ScoreListAll 
        {
            get 
            {
                var scoreList = new List<ScoreType>{new ScoreType {Name = "Все", Value = -1 }};
                scoreList.AddRange(ScoreType.GetList());
                return new SelectList(scoreList, "Value", "Name");
            }
        }

        public static IEnumerable<SelectListItem> LevelListAll {
            get
            {
                var levelList = new List<LevelType> { new LevelType { Name = "Все", Value = -1 } };
                levelList.AddRange(LevelType.GetList());
                return new SelectList(levelList, "Value", "Name");
            }
        }

        public static IEnumerable<SelectListItem> RoleListAll
        {
            get
            {
                var roleList = new List<RoleType> { new RoleType { Name = "Все", Value = -1 } };
                roleList.AddRange(RoleType.GetList());
                return new SelectList(roleList, "Value", "Name");
            }
        }
    }
}