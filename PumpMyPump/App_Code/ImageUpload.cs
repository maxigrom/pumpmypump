﻿using System;
using System.Drawing;
using System.IO;
using System.Web;
namespace PumpMyPump.App_Code
{
    public class ImageUpload
    {
        // set default size here
        public int Width { get; set; }

        public int Height { get; set; }

        public string Dir{ get; set; }

        // folder for the upload, you can put this in the web.config
        private readonly string UploadPath = "~/Images/";
        private readonly string UrlPath = "/Images/";

        public ImageUpload(int width, int height, string dir)
        {
            // TODO: Complete member initialization
            Width = width;
            Height = height;
            Dir = dir;
        }

        public ImageResult RenameUploadFile(HttpPostedFileBase file)
        {
            var fileName = Path.GetFileName(file.FileName);

            string finalFileName = Guid.NewGuid().ToString() + "_" + fileName;
            if (System.IO.File.Exists(HttpContext.Current.Request.MapPath(UploadPath + finalFileName)))
            {
                //file exists => add country try again
                return RenameUploadFile(file);
            }
            //file doesn't exist, upload item but validate first
            return UploadFile(file, finalFileName);
        }

        private ImageResult UploadFile(HttpPostedFileBase file, string fileName)
        {
            ImageResult imageResult = new ImageResult { Success = true, ErrorMessage = null };

            var path = Path.Combine(HttpContext.Current.Request.MapPath(UploadPath), Dir);
            Directory.CreateDirectory(path);

            var newFileName = Path.Combine(path, fileName);
            string extension = Path.GetExtension(file.FileName);

            //make sure the file is valid
            if (!ValidateExtension(extension))
            {
                imageResult.Success = false;
                imageResult.ErrorMessage = "Неправильный формат файла";
                return imageResult;
            }

            try
            {
                file.SaveAs(newFileName);

                Image imgOriginal = Image.FromFile(newFileName);

                //pass in whatever value you want
                Image imgActual = Scale(imgOriginal);
                imgOriginal.Dispose();
                imgActual.Save(newFileName);
                imgActual.Dispose();

                imageResult.ImageName = Path.Combine(UrlPath, Dir, fileName);

                return imageResult;
            }
            catch (Exception ex)
            {
                // you might NOT want to show the exception error for the user
                // this is generally logging or testing

                imageResult.Success = false;
                imageResult.ErrorMessage = ex.Message;
                return imageResult;
            }
        }

        private bool ValidateExtension(string extension)
        {
            extension = extension.ToLower();
            switch (extension)
            {
                case ".jpg":
                case ".png":
                case ".gif":
                case ".jpeg":
                    return true;
                default:
                    return false;
            }
        }

        private Image Scale(Image image)
        {
            var ratioX = (float)Width / image.Width;
            var ratioY = (float)Height / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(Width, Height);

            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.Clear(Color.White);

                int leftUpX = (Width - newWidth) / 2;
                int leftUpY = (Height - newHeight) / 2;

                graphics.DrawImage(image, leftUpX, leftUpY, newWidth, newHeight);
            }

            return newImage;
        }
    }

    public class ImageResult
    {
        public bool Success { get; set; }
        public string ImageName { get; set; }
        public string ErrorMessage { get; set; }
    }
}