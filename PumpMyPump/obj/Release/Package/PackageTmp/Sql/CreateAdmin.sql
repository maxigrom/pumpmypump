﻿BEGIN
   IF NOT EXISTS (SELECT * FROM [User] WHERE [Role] = 0)
   BEGIN
       INSERT INTO [User] ([Email], [Password], [Role], [Weight], [Length], [ArmSize], [Score], [Level])
       VALUES ('admin', CONVERT(VARCHAR(32), HASHBYTES('MD5', 'admin'), 2), 0, 80, 180, 80, 0, 0)
   END
END