﻿using PumpMyPump.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PumpMyPump.Controllers
{
    public class BaseAuthController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!AuthHelper.IsLoginUser)
            {
                filterContext.Result = new RedirectResult(Url.Action("Index", "LogIn"));
            }
        }
    }
}
