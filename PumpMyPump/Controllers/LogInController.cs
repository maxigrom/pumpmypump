﻿using PumpMyPump.App_Code;
using PumpMyPump.Helpers;
using PumpMyPump.Models;
using PumpMyPump.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PumpMyPump.Controllers
{
    public class LogInController : Controller
    {
        private DbDataContext db = DataBase.Connect;

        public ActionResult Index()
        {
            var loginUser = new LoginUser();

            return View(loginUser);
        }

        public ActionResult Logout()
        {
            AuthHelper.Logout();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LoginUser loginUser)
        {
            if (ModelState.IsValid)
            {
                if (AuthHelper.Login(loginUser.Email, loginUser.Password))
                {
                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError("Email", "Пара логин и пароль не совпадают.");
            }

            return View("Index", loginUser);
        }
    }
}
