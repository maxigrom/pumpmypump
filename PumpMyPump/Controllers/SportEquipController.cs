﻿using PumpMyPump.App_Code;
using PumpMyPump.Helpers;
using PumpMyPump.Models;
using PumpMyPump.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PumpMyPump.Controllers
{
    public class SportEquipController : BaseAuthController
    {
        private DbDataContext db = DataBase.Connect;

        public ActionResult Index(string sortBy = "name", bool isAsc = true)
        {
            IEnumerable<SportEquip> sportEquips = db.CurrentSportEquips;

            #region Sorting
            switch (sortBy)
            {
                default:
                    sportEquips = isAsc ? sportEquips.OrderBy(p => p.Name) : sportEquips.OrderByDescending(p => p.Name);
                    break;
            }
            #endregion

            ViewBag.SortBy = sortBy;
            ViewBag.IsAsc = isAsc;

            return View(sportEquips.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            SportEquip sportEquip = db.SportEquips.FirstOrDefault(bp => bp.Id == id);
            if (sportEquip == null)
            {
                return HttpNotFound();
            }
            return View(sportEquip);
        }

        public ActionResult Create()
        {
            var viewSportEquip = new ViewSportEquip();
            return View(viewSportEquip);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ViewSportEquip viewSportEquip)
        {
            var fileName = RenameUploadPhoto(viewSportEquip.Photo);

            if (ModelState.IsValid)
            {
                var user = AuthHelper.User;
                var sportEquip = new SportEquip
                {
                    Name = viewSportEquip.Name,
                    Photo = fileName,
                    UserId = user.IsAdmin ? 0 : user.Id
                };

                db.SportEquips.InsertOnSubmit(sportEquip);
                db.SubmitChanges();
                return RedirectToAction("Index");
            }

            return View(viewSportEquip);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var sportEquip = db.SportEquips.FirstOrDefault(bp => bp.Id == id);
            if (sportEquip == null)
            {
                return HttpNotFound();
            }

            var viewSportEquip = new ViewSportEquip
            {
                Name = sportEquip.Name,
                PhotoName = sportEquip.Photo
            };

            return View(viewSportEquip);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ViewSportEquip viewSportEquip)
        {
            var fileName = "";
            if (viewSportEquip.Photo != null)
            {
                fileName = RenameUploadPhoto(viewSportEquip.Photo);
            }

            if (ModelState.IsValid)
            {
                var dbSportEquip = db.SportEquips.FirstOrDefault(bp => bp.Id == viewSportEquip.Id);

                dbSportEquip.Name = viewSportEquip.Name;
                if (fileName.Length != 0)
                {
                    dbSportEquip.Photo = fileName;
                }

                db.SubmitChanges();
                return RedirectToAction("Index");
            }

            return View(viewSportEquip);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var sportEquip = db.SportEquips.FirstOrDefault(bp => bp.Id == id);

            if (sportEquip == null)
            {
                return HttpNotFound();
            }

            return View(sportEquip);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var sportEquip = db.SportEquips.FirstOrDefault(bp => bp.Id == id);
            db.SportEquips.DeleteOnSubmit(sportEquip);
            db.SubmitChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private string RenameUploadPhoto(HttpPostedFileBase photo)
        {
            var fileName = "";
            if (photo == null)
            {
                fileName = "/images/no_photo.png";
            }
            else
            {
                var iu = new ImageUpload(128, 128, "sportequip");
                var result = iu.RenameUploadFile(photo);

                if (result.Success)
                {
                    fileName = result.ImageName;
                }
                else
                {
                    ModelState.AddModelError("Photo", result.ErrorMessage);
                }
            }

            return fileName;
        }
    }
}
