﻿using PumpMyPump.App_Code;
using PumpMyPump.Helpers;
using PumpMyPump.Models;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PumpMyPump.Controllers
{
    public class ExerciseController : BaseAuthController
    {
        private DbDataContext db = DataBase.Connect;

        public ActionResult Index(string sortBy = "name", bool isAsc = true, string searchName = "", int searchLevel = -1, int searchScore = -1)
        {
            DataLoadOptions loadOptions = new DataLoadOptions();
            loadOptions.LoadWith<Exercise>(e => e.BodyPart);
            loadOptions.LoadWith<Exercise>(e => e.Muscle);
            loadOptions.LoadWith<Exercise>(e => e.SportEquip);
            db.LoadOptions = loadOptions;

            IEnumerable<Exercise> exercises = db.CurrentExercises;

            #region Sorting
            switch (sortBy)
            {
                case "level":
                    exercises = isAsc ? exercises.OrderBy(p => p.Level) : exercises.OrderByDescending(p => p.Level);
                    break;

                case "score":
                    exercises = isAsc ? exercises.OrderBy(p => p.Score) : exercises.OrderByDescending(p => p.Score);
                    break;

                default:
                    exercises = isAsc ? exercises.OrderBy(p => p.Name) : exercises.OrderByDescending(p => p.Name);
                    break;
            }
            #endregion

            searchName = searchName.Trim();
            if (searchName.Length != 0)
            {
                exercises = exercises.Where(t => t.Name.ToLower().Contains(searchName));
            }

            if (searchLevel != -1)
            {
                exercises = exercises.Where(t => t.Level == searchLevel);
            }

            if (searchScore != -1)
            {
                exercises = exercises.Where(t => t.Score == searchScore);
            }

            ViewBag.SortBy = sortBy;
            ViewBag.IsAsc = isAsc;

            var re = new List<Exercise>();
            if (exercises.Count() != 0)
            {
                re = exercises.ToList();
            }

            return View(re);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Exercise exercise = db.Exercises.FirstOrDefault(bp => bp.Id == id);
            if (exercise == null)
            {
                return HttpNotFound();
            }

            return View(exercise);
        }

        public ActionResult Create()
        {
            var exercise = new Exercise();

            return View(exercise);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Exercise exercise)
        {
            var fileName = RenameUploadPhoto(exercise.PhotoFile);

            if (ModelState.IsValid)
            {
                var user = AuthHelper.User;
                exercise.UserId = user.IsAdmin ? 0 : user.Id;

                exercise.Photo = fileName;
                db.Exercises.InsertOnSubmit(exercise);
                db.SubmitChanges();
                return RedirectToAction("Index");
            }

            return View(exercise);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Exercise exercise = db.Exercises.FirstOrDefault(bp => bp.Id == id);
            if (exercise == null)
            {
                return HttpNotFound();
            }

            return View(exercise);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Exercise exercise)
        {
            var fileName = "";
            if (exercise.PhotoFile != null)
            {
                fileName = RenameUploadPhoto(exercise.PhotoFile);
            }

            if (ModelState.IsValid)
            {
                Exercise dbExercise = db.Exercises.FirstOrDefault(bp => bp.Id == exercise.Id);
                dbExercise.BodyPartId = exercise.BodyPartId;
                dbExercise.MuscleId = exercise.MuscleId;
                dbExercise.SportEquipId = exercise.SportEquipId;
                dbExercise.Name = exercise.Name;
                dbExercise.Description = exercise.Description;
                dbExercise.Video = exercise.Video;
                dbExercise.Sets = exercise.Sets;
                dbExercise.Reps = exercise.Reps;
                dbExercise.Level = exercise.Level;
                dbExercise.Score = exercise.Score;

                if (fileName.Length != 0)
                {
                    dbExercise.Photo = fileName;
                }

                db.SubmitChanges();
                return RedirectToAction("Index");
            }

            return View(exercise);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Exercise exercise = db.Exercises.FirstOrDefault(bp => bp.Id == id);

            if (exercise == null)
            {
                return HttpNotFound();
            }

            return View(exercise);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Exercise exercise = db.Exercises.FirstOrDefault(bp => bp.Id == id);
            db.Exercises.DeleteOnSubmit(exercise);
            db.SubmitChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private string RenameUploadPhoto(HttpPostedFileBase photo)
        {
            var fileName = "";
            if (photo == null)
            {
                fileName = "/images/no_photo.png";
            }
            else
            {
                var iu = new ImageUpload(128, 128, "exercise");
                var result = iu.RenameUploadFile(photo);

                if (result.Success)
                {
                    fileName = result.ImageName;
                }
                else
                {
                    ModelState.AddModelError("Photo", result.ErrorMessage);
                }
            }

            return fileName;
        }
    }
}
