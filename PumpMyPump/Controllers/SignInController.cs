﻿using PumpMyPump.App_Code;
using PumpMyPump.Helpers;
using PumpMyPump.Models;
using PumpMyPump.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PumpMyPump.Controllers
{
    public class SignInController : Controller
    {
        private DbDataContext db = DataBase.Connect;

        public ActionResult Index()
        {
            var regUser = new SignInUser();

            return View(regUser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SignInUser signInUser)
        {
            if (AuthHelper.UserExists(signInUser.Email))
            {
                ModelState.AddModelError("Email", "Такой пользователь уже есть в системе.");
            }

            if (ModelState.IsValid)
            {
                var user = new User
                {
                    Email = signInUser.Email,
                    Password = AuthHelper.Crypt(signInUser.Password),
                    Weight = signInUser.Weight,
                    Length = signInUser.Length,
                    ArmSize = 9,
                    Level = signInUser.Level,
                    Score = signInUser.Score,
                    Role = 1,
                };

                db.Users.InsertOnSubmit(user);
                db.SubmitChanges();

                AuthHelper.Login(user.Email, signInUser.Password);
                return RedirectToAction("Index", "Home");
            }

            return View("Index", signInUser);
        }
    }
}
