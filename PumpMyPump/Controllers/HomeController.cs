﻿using PumpMyPump.Helpers;
using PumpMyPump.Models;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PumpMyPump.Controllers
{
    public class HomeController : Controller
    {
        private DbDataContext db = DataBase.Connect;

        public ActionResult Index(string sortBy = "name", bool isAsc = true)
        {
            var rt = new List<Training>();

            if (AuthHelper.IsLoginUser)
            {
                DataLoadOptions loadOptions = new DataLoadOptions();
                db.LoadOptions = loadOptions;

                IEnumerable<Training> trainings = db.CurrentTrainings;

                #region Sorting
                switch (sortBy)
                {
                    case "level":
                        trainings = isAsc ? trainings.OrderBy(p => p.Level) : trainings.OrderByDescending(p => p.Level);
                        break;

                    case "score":
                        trainings = isAsc ? trainings.OrderBy(p => p.Score) : trainings.OrderByDescending(p => p.Score);
                        break;

                    default:
                        trainings = isAsc ? trainings.OrderBy(p => p.Name) : trainings.OrderByDescending(p => p.Name);
                        break;
                }
                #endregion

                ViewBag.SortBy = sortBy;
                ViewBag.IsAsc = isAsc;

                trainings = trainings.Where(t => t.Score == AuthHelper.User.Score);
                trainings = trainings.Where(t => t.Level == AuthHelper.User.Level);

                if (trainings.Any())
                {
                    rt = trainings.ToList();
                }
            }

            return View(rt);
        }
    }
}
