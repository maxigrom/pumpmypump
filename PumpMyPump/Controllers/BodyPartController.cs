﻿using PumpMyPump.Helpers;
using PumpMyPump.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PumpMyPump.Controllers
{
    public class BodyPartController : BaseAuthController
    {
        private DbDataContext db = DataBase.Connect;

        public ActionResult Index(string sortBy = "name", bool isAsc = true)
        {
            IEnumerable<BodyPart> bodyParts = db.CurrentBodyParts;

            #region Sorting
            switch (sortBy)
            {
                default:
                    bodyParts = isAsc ? bodyParts.OrderBy(p => p.Name) : bodyParts.OrderByDescending(p => p.Name);
                    break;
            }
            #endregion

            ViewBag.SortBy = sortBy;
            ViewBag.IsAsc = isAsc;

            return View(bodyParts.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            BodyPart bodyPart = db.BodyParts.FirstOrDefault(bp => bp.Id == id);
            if (bodyPart == null)
            {
                return HttpNotFound();
            }
            return View(bodyPart);
        }

        public ActionResult Create()
        {
            var bodyPart = new BodyPart();
            return View(bodyPart);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BodyPart bodyPart)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = AuthHelper.User;
                    if (user.IsAdmin)
                    {
                        bodyPart.UserId = 0;
                    }
                    else
                    {
                        bodyPart.UserId = user.Id;
                    }

                    db.BodyParts.InsertOnSubmit(bodyPart);
                    db.SubmitChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                }
            }

            return View(bodyPart);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            BodyPart bodyPart = db.BodyParts.FirstOrDefault(bp => bp.Id == id);
            if (bodyPart == null)
            {
                return HttpNotFound();
            }

            return View(bodyPart);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BodyPart bodyPart)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    BodyPart dbBodyPart = db.BodyParts.FirstOrDefault(bp => bp.Id == bodyPart.Id);
                    dbBodyPart.Name = bodyPart.Name;
                    db.SubmitChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                }
            }

            return View(bodyPart);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            BodyPart bodyPart = db.BodyParts.FirstOrDefault(bp => bp.Id == id);

            if (bodyPart == null)
            {
                return HttpNotFound();
            }

            return View(bodyPart);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BodyPart bodyPart = db.BodyParts.FirstOrDefault(bp => bp.Id == id);
            db.BodyParts.DeleteOnSubmit(bodyPart);
            db.SubmitChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
