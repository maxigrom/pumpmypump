﻿using PumpMyPump.App_Code;
using PumpMyPump.Helpers;
using PumpMyPump.Models;
using PumpMyPump.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PumpMyPump.Controllers
{
    public class TrainingController : BaseAuthController
    {
        private DbDataContext db = DataBase.Connect;

        public ActionResult Index(string sortBy = "name", bool isAsc = true, string searchName = "", int searchLevel = -1, int searchScore = -1)
        {
            DataLoadOptions loadOptions = new DataLoadOptions();
            db.LoadOptions = loadOptions;

            IEnumerable<Training> trainings = db.CurrentTrainings;

            #region Sorting
            switch (sortBy)
            {
                case "level":
                    trainings = isAsc ? trainings.OrderBy(p => p.Level) : trainings.OrderByDescending(p => p.Level);
                    break;

                case "score":
                    trainings = isAsc ? trainings.OrderBy(p => p.Score) : trainings.OrderByDescending(p => p.Score);
                    break;

                default:
                    trainings = isAsc ? trainings.OrderBy(p => p.Name) : trainings.OrderByDescending(p => p.Name);
                    break;
            }
            #endregion

            searchName = searchName.Trim();
            if (searchName.Length != 0)
            {
                trainings = trainings.Where(t => t.Name.ToLower().Contains(searchName));
            }

            if (searchLevel != -1)
            {
                trainings = trainings.Where(t => t.Level == searchLevel);
            }

            if (searchScore != -1)
            {
                trainings = trainings.Where(t => t.Score == searchScore);
            }

            ViewBag.SearchName = searchName;
            ViewBag.SortBy = sortBy;
            ViewBag.IsAsc = isAsc;

            var rt = new List<Training>();
            if (trainings.Count() != 0)
            {
                rt = trainings.ToList();
            }

            return View(rt);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Training training = db.Trainings.FirstOrDefault(bp => bp.Id == id);
            if (training == null)
            {
                return HttpNotFound();
            }

            var viewTraining = new ViewTraining();
            viewTraining.Training = training;
            viewTraining.AssignExercises = GetAssignExercises(id.Value);

            return View(viewTraining);
        }

        public ActionResult GetAssignExercisePartial(int index)
        {
            var ae = new AssignExercise();
            ViewData["index"] = index;
            return PartialView("_AssignExercise", ae);
        }

        public ActionResult Create()
        {
            var viewTraining = new ViewTraining();

            return View(viewTraining);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ViewTraining viewTraining)
        {
            viewTraining.AssignExercises.RemoveAll(p => p.Deleted);

            if (ModelState.IsValid)
            {
                var user = AuthHelper.User;
                var bodyPartIds = viewTraining.AssignExercises.Select(p => p.Exercise.BodyPartId).Distinct().ToList();

                int days = Math.Min(Math.Max(viewTraining.Days, 1), bodyPartIds.Count);
                int dayIndex = 0;

                var trainings = new Training[days];
                for (int i = 0; i < bodyPartIds.Count; i++)
                {
                    if(trainings[dayIndex] == null)
                    {
                        trainings[dayIndex] = new Training() {
                            Name = string.Format("{0}. День {1}.", viewTraining.Training.Name, dayIndex + 1),
                            Level =  viewTraining.Training.Level,
                            Score =  viewTraining.Training.Score,
                            UserId = user.Id,
                            Description =  viewTraining.Training.Description
                        };

                        db.Trainings.InsertOnSubmit(trainings[dayIndex]);
                        db.SubmitChanges();
                    }

                    foreach (var ex in viewTraining.AssignExercises.Where(e => e.Exercise.BodyPartId == bodyPartIds[i]).ToList())
                    {
                        db.ExerciseTrainings.InsertOnSubmit(new ExerciseTraining
                        {
                            TrainingId = trainings[dayIndex].Id,
                            ExerciseId = ex.ExerciseId,
                            Sets = ex.Sets,
                            Reps = ex.Reps,
                            SortOrder = ex.SortOrder
                        });

                        db.SubmitChanges();
                    }

                    dayIndex++;
                    if(dayIndex >= days)
                    {
                        dayIndex = 0;
                    }
                }

                return RedirectToAction("Index");
            }

            return View(viewTraining);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Training training = db.Trainings.FirstOrDefault(bp => bp.Id == id);
            if (training == null)
            {
                return HttpNotFound();
            }

            var viewTraining = new ViewTraining();
            viewTraining.Training = training;
            viewTraining.AssignExercises = GetAssignExercises(id.Value);

            return View(viewTraining);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ViewTraining viewTraining)
        {
            var deletedExercise = viewTraining.AssignExercises.Where(p => p.Deleted && p.Id != 0).ToList();
            viewTraining.AssignExercises.RemoveAll(p => p.Deleted);

            if (ModelState.IsValid)
            {
                Training dbTraining = db.Trainings.FirstOrDefault(bp => bp.Id == viewTraining.Training.Id);
                dbTraining.Name = viewTraining.Training.Name;
                dbTraining.Description = viewTraining.Training.Description;
                dbTraining.Level = viewTraining.Training.Level;
                dbTraining.Score = viewTraining.Training.Score;
                db.SubmitChanges();

                foreach (var de in deletedExercise)
                {
                    var et = db.ExerciseTrainings.FirstOrDefault(o => o.Id == de.Id);
                    if (et != null)
                    {
                        db.ExerciseTrainings.DeleteOnSubmit(et);
                    }
                }
                db.SubmitChanges();

                foreach (var exercise in viewTraining.AssignExercises)
                {
                    var et = db.ExerciseTrainings.FirstOrDefault(o => o.Id == exercise.Id);
                    if (et != null)
                    {
                        et.ExerciseId = exercise.ExerciseId;
                        et.Sets = exercise.Sets;
                        et.Reps = exercise.Reps;
                        et.SortOrder = exercise.SortOrder;
                    }
                    else
                    {
                        db.ExerciseTrainings.InsertOnSubmit(new ExerciseTraining
                        {
                            TrainingId = viewTraining.Training.Id,
                            ExerciseId = exercise.ExerciseId,
                            Sets = exercise.Sets,
                            Reps = exercise.Reps,
                            SortOrder = exercise.SortOrder
                        });
                    }

                    db.SubmitChanges();
                }

                return RedirectToAction("Index");
            }

            return View(viewTraining);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Training training = db.Trainings.FirstOrDefault(bp => bp.Id == id);

            if (training == null)
            {
                return HttpNotFound();
            }

            return View(training);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Training training = db.Trainings.FirstOrDefault(bp => bp.Id == id);
            db.Trainings.DeleteOnSubmit(training);
            db.SubmitChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private static List<AssignExercise> GetAssignExercises(int trainingId)
        {
            var db = DataBase.Connect;
            return db.ExerciseTrainings.Where(et => et.TrainingId == trainingId).OrderBy(e => e.SortOrder).Select(et => et.ToAssignExercise()).ToList();
        }
    }
}
