﻿using PumpMyPump.Helpers;
using PumpMyPump.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PumpMyPump.Controllers
{
    public class MuscleController : BaseAuthController
    {
        private DbDataContext db = DataBase.Connect;

        public ActionResult Index(string sortBy = "name", bool isAsc = true)
        {
            IEnumerable<Muscle> muscle = db.CurrentMuscles;

            #region Sorting
            switch (sortBy)
            {
                default:
                    muscle = isAsc ? muscle.OrderBy(p => p.Name) : muscle.OrderByDescending(p => p.Name);
                    break;
            }
            #endregion

            ViewBag.SortBy = sortBy;
            ViewBag.IsAsc = isAsc;

            return View(muscle.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Muscle muscle = db.Muscles.FirstOrDefault(bp => bp.Id == id);
            if (muscle == null)
            {
                return HttpNotFound();
            }
            return View(muscle);
        }

        public ActionResult Create()
        {
            var muscle = new Muscle();
            return View(muscle);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Muscle muscle)
        {
            if (ModelState.IsValid)
            {
                var user = AuthHelper.User;
                muscle.UserId = user.IsAdmin ? 0 : user.Id;

                db.Muscles.InsertOnSubmit(muscle);
                db.SubmitChanges();
                return RedirectToAction("Index");
            }

            return View(muscle);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Muscle muscle = db.Muscles.FirstOrDefault(bp => bp.Id == id);
            if (muscle == null)
            {
                return HttpNotFound();
            }

            return View(muscle);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Muscle muscle)
        {
            if (ModelState.IsValid)
            {
                Muscle dbMuscle = db.Muscles.FirstOrDefault(bp => bp.Id == muscle.Id);
                dbMuscle.Name = muscle.Name;
                db.SubmitChanges();
                return RedirectToAction("Index");
            }

            return View(muscle);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Muscle muscle = db.Muscles.FirstOrDefault(bp => bp.Id == id);

            if (muscle == null)
            {
                return HttpNotFound();
            }

            return View(muscle);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Muscle muscle = db.Muscles.FirstOrDefault(bp => bp.Id == id);
            db.Muscles.DeleteOnSubmit(muscle);
            db.SubmitChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
