﻿using PumpMyPump.App_Code;
using PumpMyPump.Helpers;
using PumpMyPump.Models;
using PumpMyPump.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PumpMyPump.Controllers
{
    public class UserController : BaseAuthController
    {
        private DbDataContext db = DataBase.Connect;

        public ActionResult Index(string sortBy = "email", bool isAsc = true)
        {

            IEnumerable<User> users;

            #region Sorting
            switch (sortBy)
            {
                case "weight":
                    users = isAsc ? db.Users.OrderBy(p => p.Weight) : db.Users.OrderByDescending(p => p.Weight);
                    break;

                case "length":
                    users = isAsc ? db.Users.OrderBy(p => p.Length) : db.Users.OrderByDescending(p => p.Length);
                    break;

                case "role":
                    users = isAsc ? db.Users.OrderBy(p => p.Role) : db.Users.OrderByDescending(p => p.Role);
                    break;

                case "level":
                    users = isAsc ? db.Users.OrderBy(p => p.Level) : db.Users.OrderByDescending(p => p.Level);
                    break;

                case "score":
                    users = isAsc ? db.Users.OrderBy(p => p.Score) : db.Users.OrderByDescending(p => p.Score);
                    break;

                default:
                    users = isAsc ? db.Users.OrderBy(p => p.Email) : db.Users.OrderByDescending(p => p.Email);
                    break;
            }
            #endregion

            ViewBag.SortBy = sortBy;
            ViewBag.IsAsc = isAsc;

            return View(users.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = db.Users.FirstOrDefault(bp => bp.Id == id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        public ActionResult Create()
        {
            var viewUser = new ViewUser();
            return View(viewUser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ViewUser viewUser)
        {
            if (AuthHelper.UserExists(viewUser.Email))
            {
                ModelState.AddModelError("Email", "Такой пользователь уже есть в системе.");
            } 
            
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    Email = viewUser.Email,
                    Password = AuthHelper.Crypt(viewUser.Password),
                    Weight = viewUser.Weight,
                    Length = viewUser.Length,
                    ArmSize = 9,
                    Level = viewUser.Level,
                    Score = viewUser.Score,
                    Role = viewUser.Role,
                };

                db.Users.InsertOnSubmit(user);
                db.SubmitChanges();
                return RedirectToAction("Index");
            }

            return View(viewUser);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = db.Users.FirstOrDefault(bp => bp.Id == id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var viewUser = new ViewUser
            {
                Id = user.Id,
                Email = user.Email,
                Weight = user.Weight,
                Length = user.Length,
                Level = user.Level,
                Score = user.Score,
                Role = user.Role,
            };

            return View(viewUser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ViewUser viewUser)
        {
            bool needChangePassword = viewUser.Password != null && viewUser.Password.Length == 0;
            if (!needChangePassword)
            {
                ModelState.Remove("password");
            }

            if (AuthHelper.UserExists(viewUser.Email, viewUser.Id))
            {
                ModelState.AddModelError("Email", "Такой пользователь уже есть в системе.");
            } 

            if (ModelState.IsValid)
            {
                var dbUser = db.Users.FirstOrDefault(bp => bp.Id == viewUser.Id);

                dbUser.Email = viewUser.Email;
                dbUser.Weight = viewUser.Weight;
                dbUser.Length = viewUser.Length;
                dbUser.ArmSize = 9;
                dbUser.Level = viewUser.Level;
                dbUser.Score = viewUser.Score;
                dbUser.Role = viewUser.Role;

                if (needChangePassword)
                {
                    dbUser.Password = AuthHelper.Crypt(viewUser.Password);
                }

                db.SubmitChanges();
                return RedirectToAction("Index");
            }

            return View(viewUser);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = db.Users.FirstOrDefault(bp => bp.Id == id);

            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var user = db.Users.FirstOrDefault(bp => bp.Id == id);
            db.Users.DeleteOnSubmit(user);
            db.SubmitChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private string RenameUploadPhoto(HttpPostedFileBase photo)
        {
            var fileName = "";
            if (photo == null)
            {
                fileName = "/images/no_photo.png";
            }
            else
            {
                var iu = new ImageUpload(128, 128, "sportequip");
                var result = iu.RenameUploadFile(photo);

                if (result.Success)
                {
                    fileName = result.ImageName;
                }
                else
                {
                    ModelState.AddModelError("Photo", result.ErrorMessage);
                }
            }

            return fileName;
        }
    }
}
