﻿using PumpMyPump.Helpers;
using PumpMyPump.Models;
using PumpMyPump.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PumpMyPump.Controllers
{
    public class ProfileController : BaseAuthController
    {
        private DbDataContext db = DataBase.Connect;

        public ActionResult Index()
        {
            if (AuthHelper.User == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = db.Users.FirstOrDefault(bp => bp.Id == AuthHelper.User.Id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var viewUser = new ViewUser
            {
                Email = user.Email,
                Weight = user.Weight,
                Length = user.Length,
                Level = user.Level,
                Score = user.Score,
                Role = user.Role,
            };

            return View(viewUser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ViewUser viewUser)
        {
            bool needChangePassword = viewUser.Password != null && viewUser.Password.Length == 0;
            if (!needChangePassword)
            {
                ModelState.Remove("password");
            }

            if (AuthHelper.UserExists(viewUser.Email, AuthHelper.User.Id))
            {
                ModelState.AddModelError("Email", "Такой пользователь уже есть в системе.");
            }

            if (ModelState.IsValid)
            {
                var dbUser = db.Users.FirstOrDefault(bp => bp.Id == AuthHelper.User.Id);

                dbUser.Email = viewUser.Email;
                dbUser.Weight = viewUser.Weight;
                dbUser.Length = viewUser.Length;
                dbUser.ArmSize = 9;
                dbUser.Level = viewUser.Level;
                dbUser.Score = viewUser.Score;

                if (needChangePassword)
                {
                    dbUser.Password = AuthHelper.Crypt(viewUser.Password);
                }

                db.SubmitChanges();
                return RedirectToAction("Index", "Home");
            }

            return View(viewUser);
        }
    }
}
