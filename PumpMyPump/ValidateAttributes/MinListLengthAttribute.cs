﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PumpMyPump.ValidateAttributes
{
    public class MinListLengthAttribute : ValidationAttribute
    {
        private readonly int _minLength;

        public MinListLengthAttribute(int minLength)
        {
            _minLength = minLength;
        }

        public override bool IsValid(object value)
        {
            return ((IEnumerable<object>)value).Count() >= _minLength;
        }
    }
}