﻿using System.ComponentModel.DataAnnotations;

namespace PumpMyPump.Models.MetaDatas
{
    public class BodyPartMetaData
    {
        [Required(ErrorMessage = "Введите название части тела")]
        [MinLength(3, ErrorMessage = "Минимальная длина названия части тела 3 символа")]
        [MaxLength(50, ErrorMessage = "Максимальная длина названия части тела 50 символов")]
        public object Name { get; set; }
    }
}