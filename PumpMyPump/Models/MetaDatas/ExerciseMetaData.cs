﻿using PumpMyPump.ValidateAttributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PumpMyPump.Models.MetaDatas
{
    public class ExerciseMetaData
    {
        [Required(ErrorMessage = "Введите название упражнения")]
        [MinLength(3, ErrorMessage = "Минимальная длина названия упражнения 3 символа")]
        [MaxLength(50, ErrorMessage = "Максимальная длина названия упражнения 50 символов")]
        public object Name { get; set; }
        
        [Required(ErrorMessage = "Введите кол-во подходов")]
        [MinValue(1, ErrorMessage = "Минимальное кол-во подходов 1")]
        [MaxValue(99999, ErrorMessage = "Максимальное кол-во подходов 99999")]
        public object Sets { get; set; }

        [Required(ErrorMessage = "Введите кол-во повторений")]
        [MinValue(1, ErrorMessage = "Минимальное кол-во повторений 1")]
        [MaxValue(99999, ErrorMessage = "Максимальное кол-во повторений 99999")]
        public object Reps { get; set; }

        [MaxLength(2000, ErrorMessage = "Максимальная длина описания 2000 символов")]
        public object Description { get; set; }
    }
}