﻿using System.ComponentModel.DataAnnotations;

namespace PumpMyPump.Models.MetaDatas
{
    public class TrainingMetaData
    {
        [Required(ErrorMessage = "Введите название программы")]
        [MinLength(3, ErrorMessage = "Минимальная длина названия программы 3 символа")]
        [MaxLength(50, ErrorMessage = "Максимальная длина названия программы 50 символов")]
        public object Name { get; set; }

        [MaxLength(2000, ErrorMessage = "Максимальная длина описания 2000 символов")]
        public object Description { get; set; }
    }
}