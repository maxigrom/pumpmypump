﻿using System.ComponentModel.DataAnnotations;

namespace PumpMyPump.Models.MetaDatas
{
    public class SportEquipMetaData
    {
        [Required(ErrorMessage = "Введите название снаряда")]
        [MinLength(3, ErrorMessage = "Минимальная длина названия снаряда 3 символа")]
        [MaxLength(50, ErrorMessage = "Максимальная длина названия снаряда 50 символов")]
        public object Name { get; set; }
    }
}