﻿using System.ComponentModel.DataAnnotations;

namespace PumpMyPump.Models.MetaDatas
{
    public class MuscleMetaData
    {
        [Required(ErrorMessage = "Введите название мышцы")]
        [MinLength(3, ErrorMessage = "Минимальная длина названия мышцы 3 символа")]
        [MaxLength(50, ErrorMessage = "Максимальная длина названия мышцы 50 символов")]
        public object Name { get; set; }
    }
}