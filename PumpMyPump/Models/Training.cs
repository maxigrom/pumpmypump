﻿using PumpMyPump.Models.MetaDatas;
using PumpMyPump.Models.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PumpMyPump.Models
{
    [MetadataType(typeof(TrainingMetaData))]
    public partial class Training
    {
        public ScoreType ScoreType
        {
            get { return ScoreType.GetList().FirstOrDefault(s => s.Value == Score); }
        }

        public LevelType LevelType
        {
            get { return LevelType.GetList().FirstOrDefault(s => s.Value == Level); }
        }
    }
}