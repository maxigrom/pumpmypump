﻿using PumpMyPump.Models.MetaDatas;
using PumpMyPump.Models.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PumpMyPump.Models
{
    public partial class User
    {
        public decimal Index
        {
            get { return Weight / ((Length / 100) * (Length / 100)); }
        }

        public int IndexResult
        {
            get
            {
                if (Index > 25M)
                {
                    return 2;
                }

                if (Index > 18.5M)
                {
                    return 1;
                }

                return 0;
            }
        }

        public IndexType IndexType
        {
            get { return IndexType.GetList()[IndexResult]; }
        }

        public RoleType RoleType
        {
            get { return RoleType.GetList().FirstOrDefault(s => s.Value == Role); }
        }

        public ScoreType ScoreType
        {
            get { return ScoreType.GetList().FirstOrDefault(s => s.Value == Score); }
        }

        public LevelType LevelType
        {
            get { return LevelType.GetList().FirstOrDefault(s => s.Value == Level); }
        }

        public bool IsAdmin
        {
            get { return Role == 0; }
        }

        public bool IsUser
        {
            get { return Role == 1; }
        }
    }
}