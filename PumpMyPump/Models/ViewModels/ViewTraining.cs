﻿using PumpMyPump.ValidateAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PumpMyPump.Models.ViewModels
{
    public class ViewTraining
    {
        public Training Training { get; set; }

        [Required(ErrorMessage = "Введите кол-во подходов")]
        [MinListLengthAttribute(1, ErrorMessage = "Минимальное кол-во упражнений 1")]
        public List<AssignExercise> AssignExercises { get; set; }

        public int Days { get; set; }

        public ViewTraining()
        {
            Training = new Training();
            AssignExercises = new List<AssignExercise>();
        }
    }
}
