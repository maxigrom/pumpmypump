﻿using PumpMyPump.Models.MetaDatas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PumpMyPump.Models.ViewModels
{
    [MetadataType(typeof(SportEquipMetaData))]
    public class ViewSportEquip
    {
        public int Id { get; set; }

        public string Name{ get; set; }

        public HttpPostedFileBase Photo { get; set; }

        public string PhotoName { get; set; }
    }
}