﻿using PumpMyPump.ValidateAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PumpMyPump.Models.ViewModels
{
    public class SignInUser
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Введите e-mail")]
        [StringLength(50, ErrorMessage = "Длина должна быть от 5 до 50 символов", MinimumLength = 5)]
        [RegularExpression("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$", ErrorMessage = "Введите правильный email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Введите пароль")]
        [StringLength(255, ErrorMessage = "Длина должна быть от 5 до 255 символов", MinimumLength = 5)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Введите подтверждение пароля")]
        [StringLength(255, ErrorMessage = "Длина должна быть от 5 до 255 символов", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string PasswordConfirmation { get; set; }

        [Required(ErrorMessage = "Введите свой вес")]
        [MinValue(1, ErrorMessage = "Значение должено быть > 0")]
        public int Weight { get; set; }

        [Required(ErrorMessage = "Введите свой рост")]
        [MinValue(1, ErrorMessage = "Значение должено быть > 0")]
        public int Length { get; set; }
        
        public int Level { get; set; }

        public int Score { get; set; }
    }
}
