﻿using PumpMyPump.ValidateAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PumpMyPump.Models.ViewModels
{
    public class AssignExercise
    {
        public int Id { get; set; }
        public int ExerciseId { get; set; }

        private Exercise _exercise;
        public Exercise Exercise
        {
            get
            {
                if (_exercise == null)
                {
                    var db = DataBase.Connect; _exercise = db.Exercises.FirstOrDefault(e => e.Id == ExerciseId);
                }
                return _exercise;
            }
        }

        public string ExerciseName { get; set; }

        [Required(ErrorMessage = "Введите кол-во подходов")]
        [MinValue(1, ErrorMessage = "Минимальное кол-во подходов 1")]
        [MaxValue(99999, ErrorMessage = "Максимальное кол-во подходов 99999")]
        public int Sets { get; set; }

        [Required(ErrorMessage = "Введите кол-во повторений")]
        [MinValue(1, ErrorMessage = "Минимальное кол-во повторений 1")]
        [MaxValue(99999, ErrorMessage = "Максимальное кол-во повторений 99999")]
        public int Reps { get; set; }

        public int SortOrder { get; set; }
        public bool Deleted { get; set; }
    }
}