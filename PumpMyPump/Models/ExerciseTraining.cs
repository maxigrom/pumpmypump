﻿using PumpMyPump.Models.MetaDatas;
using PumpMyPump.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PumpMyPump.Models
{
    public partial class ExerciseTraining
    {
        internal AssignExercise ToAssignExercise()
        {
            return new AssignExercise
            {
                Id = Id,
                ExerciseId = ExerciseId,
                ExerciseName = Exercise.Name,
                Sets = Sets,
                Reps = Reps,
                SortOrder = SortOrder,
                Deleted = false
            };
        }
    }
}