﻿using PumpMyPump.Helpers;
using PumpMyPump.Models.MetaDatas;
using PumpMyPump.Models.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PumpMyPump.Models
{
    public partial class DbDataContext
    {
        public IQueryable<BodyPart> CurrentBodyParts
        {
            get { return BodyParts.Where(p => p.UserId == AuthHelper.User.Id || p.UserId == 0); }
        }

        public IEnumerable<Exercise> CurrentExercises
        {
            get { return Exercises.Where(p => p.UserId == AuthHelper.User.Id || p.UserId == 0); }
        }

        public IEnumerable<Muscle> CurrentMuscles
        {
            get { return Muscles.Where(p => p.UserId == AuthHelper.User.Id || p.UserId == 0); }
        }

        public IEnumerable<SportEquip> CurrentSportEquips
        {
            get { return SportEquips.Where(p => p.UserId == AuthHelper.User.Id || p.UserId == 0); }
        }

        public IEnumerable<Training> CurrentTrainings
        {
            get { return Trainings.Where(p => p.UserId == AuthHelper.User.Id || p.UserId == 0); }
        }

        partial void DeleteBodyPart(BodyPart instance)
        {
            var exercises = instance.Exercises;
            foreach (var exercise in exercises)
            {
                DeleteExercise(exercise);
            }

            ExecuteDynamicDelete(instance);
        }

        partial void DeleteMuscle(Muscle instance)
        {
            var exercises = instance.Exercises;
            foreach (var exercise in exercises)
            {
                DeleteExercise(exercise);
            }

            ExecuteDynamicDelete(instance);
        }

        partial void DeleteSportEquip(SportEquip instance)
        {
            var exercises = instance.Exercises;
            foreach (var exercise in exercises)
            {
                DeleteExercise(exercise);
            }

            ExecuteDynamicDelete(instance);
        }

        partial void DeleteExercise(Exercise instance)
        {
            var ets = instance.ExerciseTrainings;
            foreach (var e in ets)
            {
                ExecuteDynamicDelete(e);
            }

            ExecuteDynamicDelete(instance);
        }

        partial void DeleteTraining(Training instance)
        {
            var ets = instance.ExerciseTrainings;
            foreach (var e in ets)
            {
                ExecuteDynamicDelete(e);
            }

            ExecuteDynamicDelete(instance);
        }
    }
}