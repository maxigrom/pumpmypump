﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PumpMyPump.Models.Types
{
    public class ScoreType
    {
        public int Value { get; set; }

        public string Name { get; set; }

        public static List<ScoreType> GetList()
        {
            var list = new List<ScoreType>();

            list.Add(new ScoreType { Value = 0, Name = "Похудение" });
            list.Add(new ScoreType { Value = 1, Name = "Поддержание" });
            list.Add(new ScoreType { Value = 2, Name = "Набор массы" }); 

            return list;
        }
    }
}