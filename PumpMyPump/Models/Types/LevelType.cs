﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PumpMyPump.Models.Types
{
    public class LevelType
    {
        public int Value { get; set; }

        public string Name { get; set; }

        public static List<LevelType> GetList()
        {
            var list = new List<LevelType>();

            list.Add(new LevelType { Value = 0, Name = "Начальный" });
            list.Add(new LevelType { Value = 1, Name = "Продвинутый" });
            list.Add(new LevelType { Value = 2, Name = "Мастер" }); 

            return list;
        }
    }
}