﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PumpMyPump.Models.Types
{
    public class IndexType
    {
        public int Value { get; set; }

        public string Name { get; set; }

        public static List<IndexType> GetList()
        {
            var list = new List<IndexType>();

            list.Add(new IndexType { Value = 0, Name = "Избыточный вес" });
            list.Add(new IndexType { Value = 1, Name = "Норма" });
            list.Add(new IndexType { Value = 2, Name = "Недостаток массы" }); 

            return list;
        }
    }
}