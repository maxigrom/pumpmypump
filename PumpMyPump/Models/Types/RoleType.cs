﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PumpMyPump.Models.Types
{
    public class RoleType
    {
        public int Value { get; set; }

        public string Name { get; set; }

        public static List<RoleType> GetList()
        {
            var list = new List<RoleType>();

            list.Add(new RoleType { Value = 0, Name = "Администратор" });
            list.Add(new RoleType { Value = 1, Name = "Пользователь" });

            return list;
        }
    }
}